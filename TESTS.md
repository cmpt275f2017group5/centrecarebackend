# Test Cases

## /api/session:  

** ERROR CONDITIONS **  
POST    invalid username / password  
GET     no session token  
GET     invalid session token  
DELETE  no session token  
DELETE  invalid session token  

** CORRECT CONDITIONS **  
POST    correct username / password  
GET     correct session token  
DELETE  correct session token  



## /api/patient{pid}:  

** ERROR CONDITIONS **  
REQUEST with non-integer pid  



## /api/patient/{pid}/profile:  

** ERROR CONDITIONS **  
GET     patient with no token  
GET     patient with non-associative token  
PUT     patient with no token  
PUT     patient with non-associative token  

** CORRECT CONDITIONS **  
GET     patient with correct token  
PUT     patient with correct token  



## /api/patient/{pid}/location:  

** ERROR CONDITIONS **  
GET     location with no token  
GET     location with non-associative token  
PUT     patient with no token  
PUT     patient with non-associative token  

** CORRECT CONDITIONS **  
GET     location with correct token  
PUT     location with correct token  



## /api/caretaker/{cid}:  

** ERROR CONDITIONS **  
REQUEST with non-integer cid  



## /api/caretaker/{cid}/profile:

** ERROR CONDITIONS **  
GET     caretaker with no token  
GET     caretaker with non-associative token  
PUT     caretaker with no token  
PUT     caretaker with non-associative token  

** CORRECT CONDITIONS **  
GET     caretaker with correct token  
PUT     caretaker with correct token  