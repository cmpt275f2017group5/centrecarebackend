/**
 *  
 *  SessionChecker.java
 *  
 *  
 *
 *  @author     First Last
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.servlets;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.centrecare.services.SessionService;

@WebListener
public class SessionChecker implements ServletContextListener, Runnable {
	// XXX should get these values from configuration
	private static final long CHECK_INITIAL_DELAY_MIN = 0;
	private static final long CHECK_PERIOD_MIN = 1;
	private static final long SESSION_TIMEOUT_PERIOD_SEC = 1800;

	private ScheduledExecutorService scheduler;

	public SessionChecker() {
	}

	public void contextDestroyed(ServletContextEvent event) {
		scheduler.shutdownNow();
	}

	public void contextInitialized(ServletContextEvent event) {
		event.getServletContext().log("Starting SessionChecker ...");
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(this, CHECK_INITIAL_DELAY_MIN, CHECK_PERIOD_MIN, TimeUnit.MINUTES);
	}

	@Override
	public void run() {
		SessionService.checkSessionExpired(SESSION_TIMEOUT_PERIOD_SEC);
	}

}
