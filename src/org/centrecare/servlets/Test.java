package org.centrecare.servlets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.centrecare.dao.CredentialsDao;
import org.centrecare.dao.DaoException;
import org.centrecare.types.UserRole;


@WebServlet("/test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource(name = "jdbc/fence")
	private javax.sql.DataSource ds;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			CredentialsDao dao = new CredentialsDao();
			UserRole userRole = dao.getUserRole ("ron");
			response.getWriter().append(userRole.getTitle());
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
