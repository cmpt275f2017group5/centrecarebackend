package org.centrecare.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.centrecare.dao.DaoException.Reason;
import org.centrecare.types.UserRole;

public class Test {
	Connection connection;

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		System.out.println("!!!!!!!");
		try {
			// new Test().runSessionContext();
			new Test().runCredentials();
		} catch (DaoException e) {
			System.out.println("DaoException: reason=" + e.getReason());

			if (e.getReason() == Reason.CAUSE) {
				System.out.println("cause message=" + e.getCause().getMessage());
			}
		}
	}

	private Test() throws ClassNotFoundException, SQLException {
		Class.forName("org.mariadb.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3306/fence");
	}

	private void runSessionContext() throws DaoException {
		for (int i = 0; i < 1000; i++) {
			SessionContextDao dao = new SessionContextDao();
			dao.setConnection(connection);
			dao.createSessionContext("AAA-" + Integer.toString(i), UserRole.PATIENT, 123);
			System.out.println(i);
		}

		// dao.createSessionContext("111-222-333", UserRole.ADMIN, 123);
		// dao.setAttribute("111-222-333", "ready", "true");
		// dao.removeAttribute("111-222-333", "ready");

		// System.out.println(dao.getSessionContext("111-222-333"));
		// dao.updateLastAccess("111-222-333");
		// System.out.println(dao.getSessionContext("111-222-333"));

	}

	private void runCredentials() throws DaoException {
		CredentialsDao dao = new CredentialsDao();
		dao.setConnection(connection);
		
		System.out.println(dao.validate("ron",  "secret"));
		System.out.println(dao.getUserRole("ron"));
		System.out.println(dao.getUserId("ron"));

	}
}
