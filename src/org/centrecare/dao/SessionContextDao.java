/**
 *  
 *  SessionContextDao.java
 *  
 *  Accesses fields in the "session_context" table. 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.centrecare.dao.DaoException.Reason;
import org.centrecare.entities.SessionContext;
import org.centrecare.types.UserRole;

public class SessionContextDao implements AutoCloseable {
	private static final String SESSION_CONTEXT_TABLE_NAME = "session_context";
	private static final String SESSION_CONTEXT_ATTR_TABLE_NAME = "session_context_attr";

	private static final String ACCESS_TOKEN_FIELD_NAME = "access_token";
	private static final String USER_ROLE_FIELD_NAME = "user_role";
	private static final String USER_ID_FIELD_NAME = "user_id";
	private static final String LAST_ACCESS_FIELD_NAME = "last_access";
	private static final String KEY_FIELD_NAME = "attr_key";
	private static final String VALUE_FIELD_NAME = "attr_value";

	private Connection c;
	private final Date now;

	public SessionContextDao() throws DaoException {
		c = DaoHelper.getConnection();
		now = new Date();
	}

	public void close() {
		DaoHelper.close(c);
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void createSessionContext(String accessToken, UserRole userRole, int userId) throws DaoException {
		StringBuilder sql = new StringBuilder().append("INSERT INTO ").append(SESSION_CONTEXT_TABLE_NAME)
				.append(" VALUES(?,?,?,?)");

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, accessToken);
			p.setString(2, userRole.toString());
			p.setInt(3, userId);
			p.setTimestamp(4, new java.sql.Timestamp(now.getTime()));
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}

		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
				throw new DaoException(Reason.ALREADY_EXISTS);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public SessionContext getSessionContext(String accessToken) throws DaoException {
		StringBuilder sql = new StringBuilder().append("SELECT ").append(USER_ROLE_FIELD_NAME).append(", ")
				.append(USER_ID_FIELD_NAME).append(", ").append(LAST_ACCESS_FIELD_NAME).append(" FROM ")
				.append(SESSION_CONTEXT_TABLE_NAME).append(" WHERE ").append(ACCESS_TOKEN_FIELD_NAME).append("=?");

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, accessToken);
			rs = p.executeQuery();
			if (rs.next()) {
				// get context
				SessionContext ctx = new SessionContext(accessToken, rs.getString(USER_ROLE_FIELD_NAME),
						rs.getInt(USER_ID_FIELD_NAME), new Date(rs.getTimestamp(LAST_ACCESS_FIELD_NAME).getTime()));

				// add attributes
				ctx.getAttribute().putAll(getAllAttributes(accessToken));
				return ctx;
			}
			throw new DaoException(Reason.DOES_NOT_EXIST);
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public void removeSessionContext(String accessToken) throws DaoException {
		StringBuilder sql = new StringBuilder().append("DELETE FROM ").append(SESSION_CONTEXT_TABLE_NAME)
				.append(" WHERE ").append(ACCESS_TOKEN_FIELD_NAME).append("=?");

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, accessToken);
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public void removeAllSessionContexts(UserRole userRole, int userId) throws DaoException {
		StringBuilder sql = new StringBuilder().append("DELETE FROM ").append(SESSION_CONTEXT_TABLE_NAME)
				.append(" WHERE ").append(USER_ROLE_FIELD_NAME).append("=? AND ").append(USER_ID_FIELD_NAME)
				.append("=?");

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, userRole.toString());
			p.setInt(2, userId);
			p.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public void updateLastAccess(String accessToken) throws DaoException {
		StringBuilder sql = new StringBuilder().append("UPDATE ").append(SESSION_CONTEXT_TABLE_NAME).append(" SET ")
				.append(LAST_ACCESS_FIELD_NAME).append("=?").append(" WHERE ").append(ACCESS_TOKEN_FIELD_NAME)
				.append("=?");

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setTimestamp(1, new java.sql.Timestamp(now.getTime()));
			p.setString(2, accessToken);
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public void setAttribute(String accessToken, String key, String value) throws DaoException {
		StringBuilder sql = new StringBuilder().append("REPLACE INTO ").append(SESSION_CONTEXT_ATTR_TABLE_NAME)
				.append(" VALUES(?,?,?)");

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, accessToken);
			p.setString(2, key);
			p.setString(3, value);
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}

		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_NO_REFERENCED_ROW_2) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public void removeAttribute(String accessToken, String key) throws DaoException {
		StringBuilder sql = new StringBuilder().append("DELETE FROM ").append(SESSION_CONTEXT_ATTR_TABLE_NAME)
				.append(" WHERE ").append(ACCESS_TOKEN_FIELD_NAME).append("=? AND ").append(KEY_FIELD_NAME)
				.append("=?");

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, accessToken);
			p.setString(2, key);
			p.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public String getAttribute(String accessToken, String key) throws DaoException {
		StringBuilder sql = new StringBuilder().append("SELECT ").append(VALUE_FIELD_NAME).append(" FROM ")
				.append(SESSION_CONTEXT_ATTR_TABLE_NAME).append(" WHERE ").append(ACCESS_TOKEN_FIELD_NAME)
				.append("=? AND ").append(KEY_FIELD_NAME).append("=?");

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, accessToken);
			p.setString(2, key);
			rs = p.executeQuery();
			if (rs.next()) {
				return rs.getString(VALUE_FIELD_NAME);
			}
			throw new DaoException(Reason.NOT_FOUND);
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public Map<String, String> getAllAttributes(String accessToken) throws DaoException {
		StringBuilder sql = new StringBuilder().append("SELECT ").append(KEY_FIELD_NAME).append(", ")
				.append(VALUE_FIELD_NAME).append(" FROM ").append(SESSION_CONTEXT_ATTR_TABLE_NAME).append(" WHERE ")
				.append(ACCESS_TOKEN_FIELD_NAME).append("=?");

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, accessToken);
			rs = p.executeQuery();
			Map<String, String> attrs = new HashMap<>();
			while (rs.next()) {
				attrs.put(rs.getString(KEY_FIELD_NAME), rs.getString(VALUE_FIELD_NAME));
			}
			return attrs;
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public void expireSessions(long period) throws DaoException {
		long expiryTimestamp = now.getTime() - (period * 1000);
		String sql = "DELETE FROM " + SESSION_CONTEXT_TABLE_NAME + " WHERE " + LAST_ACCESS_FIELD_NAME + "<?";

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setTimestamp(1, new Timestamp(expiryTimestamp));
			p.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
			// silently ignore
		} finally {
			DaoHelper.close(p);
		}
	}

}
