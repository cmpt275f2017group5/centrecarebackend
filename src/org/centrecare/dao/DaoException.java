/**
 *  
 *  DaoException.java
 *  
 *  Exception pertaining to accessing fields in database.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

public class DaoException extends Throwable {
	private static final long serialVersionUID = 1L;
	private final Reason reason;

	public DaoException(Reason reason) {
		this.reason = reason;
	}

	public DaoException(Reason reason, String message) {
		super(message);
		this.reason = reason;
	}

	public DaoException(Reason reason, Throwable cause) {
		super(cause);
		this.reason = reason;
	}

	public DaoException(Reason reason, String message, Throwable cause) {
		super(message, cause);
		this.reason = reason;
	}

	public Reason getReason() {
		return reason;
	}

	public enum Reason {
		UNKNOWN, DOES_NOT_EXIST, ALREADY_EXISTS, CAUSE, NOT_FOUND;
	}

}
