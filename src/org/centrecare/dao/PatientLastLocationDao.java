/**
 *  
 *  CredentialsDao.java
 *  
 *  Accesses fields in the "credentials" table.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.centrecare.dao.DaoException.Reason;
import org.centrecare.entities.PatientLastLocation;
import org.centrecare.entities.PatientLocation;

public class PatientLastLocationDao implements AutoCloseable {
	private static final String TABLE_NAME = "patient_last_location";
	private static final String PATIENT_ID_FIELD_NAME = "patient_id";
	private static final String LONGITUDE_FIELD_NAME = "longitude";
	private static final String LATITUDE_FIELD_NAME = "latitude";
	private static final String TIMESTAMP_FIELD_NAME = "time";

	private Connection c;

	public PatientLastLocationDao() throws DaoException {
		c = DaoHelper.getConnection();
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void close() {
		DaoHelper.close(c);
	}

	public void updateLocation(int patient_id, PatientLocation location) throws DaoException {
		String sql = "REPLACE INTO " + TABLE_NAME + " VALUES(?,?,?,?)";
		Date now = new Date();

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setInt(1, patient_id);
			p.setDouble(2, location.getLongitude());
			p.setDouble(3, location.getLatitude());
			p.setTimestamp(4, new Timestamp(now.getTime()));
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}

		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_NO_REFERENCED_ROW_2) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public PatientLastLocation getLocation(int patient_id) throws DaoException {
		String sql = "SELECT " + LONGITUDE_FIELD_NAME + ", " + LATITUDE_FIELD_NAME + ", " + TIMESTAMP_FIELD_NAME
				+ " FROM " + TABLE_NAME + " WHERE " + PATIENT_ID_FIELD_NAME + "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql);
			p.setInt(1, patient_id);
			rs = p.executeQuery();
			if (rs.next()) {
				return new PatientLastLocation(rs.getDouble(LONGITUDE_FIELD_NAME), rs.getDouble(LATITUDE_FIELD_NAME),
						new Date(rs.getTimestamp(TIMESTAMP_FIELD_NAME).getTime()));
			}
			throw new DaoException(Reason.DOES_NOT_EXIST);
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

}
