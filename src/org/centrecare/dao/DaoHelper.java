/**
 *  
 *  DaoHelper.java
 *  
 *  Helper class for accessing database. 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DaoHelper {
	public static final int ER_NO_REFERENCED_ROW_2 = 1452;
	public static final int ER_DUP_ENTRY = 1062;

	private DaoHelper() {
	}

	public static Connection getConnection() {
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:/comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/fence");
			return ds.getConnection();
		} catch (NamingException | SQLException e) {
			System.err.println("Failed to lookup JNDI data source: " + e.getMessage());
			return null;
		}

	}

	public static void close(AutoCloseable ac) {
		if (ac != null) {
			try {
				ac.close();
			} catch (Exception e) {
				// silently ignore
			}
		}
	}

//	public static void close(Connection c) {
//		if (c != null) {
//			try {
//				c.close();
//			} catch (SQLException e) {
//				// silently ignore
//			}
//		}
//	}
//
//	public static void close(PreparedStatement p) {
//		if (p != null) {
//			try {
//				p.close();
//			} catch (SQLException e) {
//				// silently ignore
//			}
//		}
//	}
//
//	public static void close(ResultSet rs) {
//		if (rs != null) {
//			try {
//				rs.close();
//			} catch (SQLException e) {
//				// silently ignore
//			}
//		}
//	}

}
