/**
 *  
 *  PairingCodeDao.java
 *  
 *  Accesses fields in the "paircode" table.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.centrecare.entities.Geofence;
import org.centrecare.dao.DaoException;
import org.centrecare.dao.DaoException.Reason;
import org.centrecare.dao.DaoHelper;

public class PairingCodeDao implements AutoCloseable {
	private static final String TABLE_NAME = "paircode";
    private static final String CODE_FIELD_NAME = "code";
    private static final String CARETAKER_ID_FIELD_NAME = "caretaker_id";
    private static final String TIME_CREATED_FIELD_NAME = "time_created";

	private Connection c;

	public PairingCodeDao() throws DaoException {
		c = DaoHelper.getConnection();
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void close() {
		DaoHelper.close(c);
	}

    public void createPairingCode(String pairCode, int caretaker_id) throws DaoException {
        String sql = "INSERT INTO " + TABLE_NAME + " (" + CODE_FIELD_NAME + "," + CARETAKER_ID_FIELD_NAME + ") VALUES(?,?)";

        PreparedStatement p = null;
        try {
            p = c.prepareStatement(sql);
            p.setString(1, pairCode);
            p.setInt(2, caretaker_id);

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                throw new DaoException(Reason.UNKNOWN);
            }
        } catch(SQLException e) {
            System.out.println(e.getErrorCode());
            if(e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
                throw new DaoException(Reason.ALREADY_EXISTS);
            }

            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(p);
        }
    }

    public String getPairingCode(int caretaker_id) throws DaoException {
        String sql = "SELECT " + CODE_FIELD_NAME + ", "+ TIME_CREATED_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + CARETAKER_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        try {
            //TODO: Check Timestamp
            
            p = c.prepareStatement(sql);
            p.setInt(1, caretaker_id);

            rs = p.executeQuery();

            if(rs.next()) {
                return rs.getString(CODE_FIELD_NAME);
            }
            
            throw new DaoException(Reason.DOES_NOT_EXIST);
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }

    public int usePairingCode(String pairCode) throws DaoException {
        String sqlSelect = "SELECT " + CARETAKER_ID_FIELD_NAME + ", " + TIME_CREATED_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + CODE_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        int caretaker_id;
        try {
            //TODO: Check Timestamp

            p = c.prepareStatement(sqlSelect);
            p.setString(1, pairCode);

            rs = p.executeQuery();

            if(rs.next()) {
                caretaker_id = rs.getInt(CARETAKER_ID_FIELD_NAME);
            } else {
                throw new DaoException(Reason.DOES_NOT_EXIST);
            }
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }

        String sqlDelete = "DELETE FROM " + TABLE_NAME + " WHERE " + CODE_FIELD_NAME + "=? AND " + CARETAKER_ID_FIELD_NAME + "=?";

        try {
            p = c.prepareStatement(sqlDelete);
            p.setString(1, pairCode);
            p.setInt(2, caretaker_id);

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                throw new DaoException(Reason.UNKNOWN);
            }
        } catch(SQLException e) {
            throw new DaoException(Reason.UNKNOWN);
        } finally {
            DaoHelper.close(p);
        }

        return caretaker_id;
    }
}    
