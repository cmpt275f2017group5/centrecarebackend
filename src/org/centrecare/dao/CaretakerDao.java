/**
 *  
 *  CredentialsDao.java
 *  
 *  Accesses fields in the "caretaker" table.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.centrecare.dao.DaoException.Reason;
import org.centrecare.entities.Address;
import org.centrecare.entities.Caretaker;

public class CaretakerDao implements AutoCloseable {
	private static final String TABLE_NAME = "caretaker";
	private static final String ID_FIELD_NAME = "caretaker_id";
	private static final String FIRSTN_FIELD_NAME = "first_name";
	private static final String LASTN_FIELD_NAME = "last_name";
	private static final String PHONENO_FIELD_NAME = "phone_number";
	private static final String STREET_FIELD_NAME = "street";
	private static final String CITY_FIELD_NAME = "city";
	private static final String PROV_FIELD_NAME = "province";
	private static final String COUNTRY_FIELD_NAME = "country";
	private static final String POST_FIELD_NAME = "postal_code";
	private static final String EMAIL_FIELD_NAME = "email";

	private Connection c;

	public CaretakerDao() throws DaoException {
		c = DaoHelper.getConnection();
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void close() {
		DaoHelper.close(c);
	}

	public int createCaretaker(Caretaker pat) throws DaoException {
		String sql = "INSERT INTO " + TABLE_NAME + " VALUES(null,?,?,?,?,?,?,?,?,?)";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			p.setString(1, pat.getFirstName());
			p.setString(2, pat.getLastName());
			p.setString(3, pat.getPhoneNo());
			p.setString(4, pat.getAddress().getStreet());
			p.setString(5, pat.getAddress().getCity());
			p.setString(6, pat.getAddress().getProvince());
			p.setString(7, pat.getAddress().getCountry());
			p.setString(8, pat.getAddress().getPostalCode());
			p.setString(9, pat.getEmail());

			if (p.executeUpdate() == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}

			rs = p.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
			throw new DaoException(Reason.UNKNOWN);
		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
				throw new DaoException(Reason.ALREADY_EXISTS);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public Caretaker getCaretaker(int id) throws DaoException {
		String sql = "SELECT " + FIRSTN_FIELD_NAME + ", " + LASTN_FIELD_NAME + ", " + PHONENO_FIELD_NAME + ", "
				+ STREET_FIELD_NAME + ", " + CITY_FIELD_NAME + ", " + PROV_FIELD_NAME + ", " + COUNTRY_FIELD_NAME + ", "
				+ POST_FIELD_NAME + ", " + EMAIL_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + ID_FIELD_NAME + "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql);
			p.setInt(1, id);
			rs = p.executeQuery();
			if (rs.next()) {
				String firstName = rs.getString(FIRSTN_FIELD_NAME);
				String lastName = rs.getString(LASTN_FIELD_NAME);
				String phoneNo = rs.getString(PHONENO_FIELD_NAME);
				String street = rs.getString(STREET_FIELD_NAME);
				String city = rs.getString(CITY_FIELD_NAME);
				String province = rs.getString(PROV_FIELD_NAME);
				String country = rs.getString(COUNTRY_FIELD_NAME);
				String postalCode = rs.getString(POST_FIELD_NAME);
				String email = rs.getString(EMAIL_FIELD_NAME);

				Address address = new Address(street, city, province, country, postalCode);
				return new Caretaker(firstName, lastName, phoneNo, address, email);
			}
			throw new DaoException(Reason.DOES_NOT_EXIST);
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public void updateCaretaker(int id, Caretaker pat) throws DaoException {
		String sql = "UPDATE " + TABLE_NAME + " SET " + FIRSTN_FIELD_NAME + "=?, " + LASTN_FIELD_NAME + "=?, "
				+ PHONENO_FIELD_NAME + "=?, " + STREET_FIELD_NAME + "=?, " + CITY_FIELD_NAME + "=?, " + PROV_FIELD_NAME
				+ "=?, " + COUNTRY_FIELD_NAME + "=?, " + POST_FIELD_NAME + "=?, " + EMAIL_FIELD_NAME + "=? WHERE "
				+ ID_FIELD_NAME + "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			p.setString(1, pat.getFirstName());
			p.setString(2, pat.getLastName());
			p.setString(3, pat.getPhoneNo());
			p.setString(4, pat.getAddress().getStreet());
			p.setString(5, pat.getAddress().getCity());
			p.setString(6, pat.getAddress().getProvince());
			p.setString(7, pat.getAddress().getCountry());
			p.setString(8, pat.getAddress().getPostalCode());
			p.setString(9, pat.getEmail());
			p.setInt(10, id);

			if (p.executeUpdate() == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
				throw new DaoException(Reason.ALREADY_EXISTS);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public void removeCaretaker(int id) throws DaoException {
		String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + ID_FIELD_NAME + "=?";

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql);
			p.setInt(1, id);
			int count = p.executeUpdate();
			if (count == 0) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

}
