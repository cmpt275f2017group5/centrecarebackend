/**
 *  
 *  CredentialsDao.java
 *  
 *  Accesses fields in the "credentials" table.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.centrecare.dao.DaoException.Reason;
import org.centrecare.types.UserRole;
import org.centrecare.types.UserStatus;

public class CredentialsDao implements AutoCloseable {
	private static final String TABLE_NAME = "credentials";
	private static final String USERNAME_FIELD_NAME = "username";
	private static final String PASSWORD_FIELD_NAME = "password";
	private static final String USER_ROLE_FIELD_NAME = "user_role";
	private static final String USER_ID_FIELD_NAME = "user_id";

	private Connection c;

	// @Resource(name = "jdbc/fence")
	// private javax.sql.DataSource ds;

	public CredentialsDao() throws DaoException {
		c = DaoHelper.getConnection();
		// try {
		// Context initCtx = new InitialContext();
		// Context envCtx = (Context) initCtx.lookup("java:comp/env");
		// ds = (DataSource) envCtx.lookup("jdbc/fence");
		// } catch (NamingException e) {
		// System.err.println("Failed to lookup JNDI data source");
		// }
		//
		// if (ds != null) {
		// try {
		// c = ds.getConnection();
		// } catch (SQLException e) {
		// throw new DaoException(Reason.CAUSE, e);
		// }
		// }
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void close() {
		DaoHelper.close(c);
	}

	public void createCredentials(String username, String password, UserRole userRole, int userId, UserStatus userStatus) throws DaoException {
		String sql = "INSERT INTO " + TABLE_NAME + " VALUES(?,?,?,?,?)";

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql);
			p.setString(1, username);
			p.setString(2, password);
			p.setString(3, userRole.toString());
			p.setInt(4, userId);
			p.setString(5, userStatus.toString());
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}

		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
				throw new DaoException(Reason.ALREADY_EXISTS);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public UserRole getUserRole(String username) throws DaoException {
		String sql = "SELECT " + USER_ROLE_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + USERNAME_FIELD_NAME + "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql);
			p.setString(1, username);
			rs = p.executeQuery();
			if (rs.next()) {
				// get context
				return UserRole.valueOf(rs.getString(USER_ROLE_FIELD_NAME));
			}
			throw new DaoException(Reason.DOES_NOT_EXIST);
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public int getUserId(String username) throws DaoException {
		String sql = "SELECT " + USER_ID_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + USERNAME_FIELD_NAME + "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql);
			p.setString(1, username);
			rs = p.executeQuery();
			if (rs.next()) {
				// get context
				return rs.getInt(USER_ID_FIELD_NAME);
			}
			throw new DaoException(Reason.DOES_NOT_EXIST);
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public void removeCredentials(String username) throws DaoException {
		String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + USERNAME_FIELD_NAME + "=?";

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, username);
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public void updatePassword(String username, String password) throws DaoException {
		String sql = "UPDATE " + TABLE_NAME + " SET " + PASSWORD_FIELD_NAME + "=? WHERE " + USERNAME_FIELD_NAME + "=?";

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql.toString());
			p.setString(1, username);
			p.setString(2, password);
			int rowsUpdated = p.executeUpdate();
			if (rowsUpdated == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}

		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_NO_REFERENCED_ROW_2) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

	public boolean validate(String username, String password) throws DaoException {
		String sql = "SELECT 1 FROM " + TABLE_NAME + " WHERE " + USERNAME_FIELD_NAME + "=? AND " + PASSWORD_FIELD_NAME
				+ "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql);
			p.setString(1, username);
			p.setString(2, password);
			rs = p.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}
    
    public boolean checkUsernameExists(String username) throws DaoException {
        String sql = "SELECT 1 FROM " + TABLE_NAME + " WHERE " + USERNAME_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        try {
            p = c.prepareStatement(sql);
            p.setString(1, username);
            
            rs = p.executeQuery();
            return rs.next();
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }
}
