/**
 *  
 *  PatientDao.java
 *  
 *  Accesses fields in the "patient" table.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import org.centrecare.dao.DaoException.Reason;
import org.centrecare.entities.Address;
import org.centrecare.entities.Patient;

public class PatientDao implements AutoCloseable {
	private static final String TABLE_NAME = "patient";
	private static final String ID_FIELD_NAME = "patient_id";
	private static final String FIRSTN_FIELD_NAME = "first_name";
	private static final String LASTN_FIELD_NAME = "last_name";
	private static final String PHONENO_FIELD_NAME = "phone_number";
	private static final String DOB_FIELD_NAME = "date_of_birth";
	private static final String STREET_FIELD_NAME = "street";
	private static final String CITY_FIELD_NAME = "city";
	private static final String PROV_FIELD_NAME = "province";
	private static final String COUNTRY_FIELD_NAME = "country";
	private static final String POST_FIELD_NAME = "postal_code";
	private static final String RELATE_FIELD_NAME = "relationship_to_caretaker";
	private static final String CARETAKER_ID_FIELD_NAME = "caretaker_id";

	private Connection c;

	public PatientDao() throws DaoException {
		c = DaoHelper.getConnection();
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void close() {
		DaoHelper.close(c);
	}

	public int createPatient(Patient pat) throws DaoException {
		String sql = "INSERT INTO " + TABLE_NAME + " VALUES(null,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			p.setString(1, pat.getFirstName());
			p.setString(2, pat.getLastName());
			p.setString(3, pat.getPhoneNo());
			p.setDate(4, new java.sql.Date(pat.getDateofBirth().getTime()));
			p.setString(5, pat.getAddress().getStreet());
			p.setString(6, pat.getAddress().getCity());
			p.setString(7, pat.getAddress().getProvince());
			p.setString(8, pat.getAddress().getCountry());
			p.setString(9, pat.getAddress().getPostalCode());
			p.setString(10, "");
			p.setInt(11, pat.getCaretakerId());

			if (p.executeUpdate() == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}

			rs = p.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
			throw new DaoException(Reason.UNKNOWN);
		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
				throw new DaoException(Reason.ALREADY_EXISTS);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public Patient getPatient(int id) throws DaoException {
		String sql = "SELECT " + FIRSTN_FIELD_NAME + ", " + LASTN_FIELD_NAME + ", " + PHONENO_FIELD_NAME + ", "
				+ DOB_FIELD_NAME + ", " + STREET_FIELD_NAME + ", " + CITY_FIELD_NAME + ", " + PROV_FIELD_NAME + ", "
				+ COUNTRY_FIELD_NAME + ", " + POST_FIELD_NAME + ", " + RELATE_FIELD_NAME + ", "
				+ CARETAKER_ID_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + ID_FIELD_NAME + "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql);
			p.setInt(1, id);
			rs = p.executeQuery();
			if (rs.next()) {
				String firstName = rs.getString(FIRSTN_FIELD_NAME);
				String lastName = rs.getString(LASTN_FIELD_NAME);
				String phoneNo = rs.getString(PHONENO_FIELD_NAME);
				Date dateofBirth = new Date(rs.getDate(DOB_FIELD_NAME).getTime());
				String street = rs.getString(STREET_FIELD_NAME);
				String city = rs.getString(CITY_FIELD_NAME);
				String province = rs.getString(PROV_FIELD_NAME);
				String country = rs.getString(COUNTRY_FIELD_NAME);
				String postalCode = rs.getString(POST_FIELD_NAME);
				int caretakerId = rs.getInt(CARETAKER_ID_FIELD_NAME);

				Address address = new Address(street, city, province, country, postalCode);
				return new Patient(firstName, lastName, phoneNo, address, dateofBirth, caretakerId);
			}
			throw new DaoException(Reason.DOES_NOT_EXIST);
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

    public List<Integer> getPatientIdList(int caretaker_id) throws DaoException {
        String sql = "SELECT " + ID_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + CARETAKER_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        List<Integer> patientList = new ArrayList<Integer>();
        try {
            p = c.prepareStatement(sql);
            p.setInt(1, caretaker_id);
            
            rs = p.executeQuery();
            while(rs.next()) {
                patientList.add(rs.getInt(ID_FIELD_NAME));
            }

            return patientList;
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }

	public void updatePatient(int id, Patient pat) throws DaoException {
		String sql = "UPDATE " + TABLE_NAME + " SET " + FIRSTN_FIELD_NAME + "=?, " + LASTN_FIELD_NAME + "=?, "
				+ PHONENO_FIELD_NAME + "=?, " + DOB_FIELD_NAME + "=?, " + STREET_FIELD_NAME + "=?, " + CITY_FIELD_NAME
				+ "=?, " + PROV_FIELD_NAME + "=?, " + COUNTRY_FIELD_NAME + "=?, " + POST_FIELD_NAME + "=?, "
				+ RELATE_FIELD_NAME + "=?, " + CARETAKER_ID_FIELD_NAME + "=? WHERE " + ID_FIELD_NAME + "=?";

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			p.setString(1, pat.getFirstName());
			p.setString(2, pat.getLastName());
			p.setString(3, pat.getPhoneNo());
			p.setDate(4, new java.sql.Date(pat.getDateofBirth().getTime()));
			p.setString(5, pat.getAddress().getStreet());
			p.setString(6, pat.getAddress().getCity());
			p.setString(7, pat.getAddress().getProvince());
			p.setString(8, pat.getAddress().getCountry());
			p.setString(9, pat.getAddress().getPostalCode());
			p.setString(10, "");
			p.setInt(11, pat.getCaretakerId());
			p.setInt(12, id);

			if (p.executeUpdate() == 0) {
				throw new DaoException(Reason.UNKNOWN);
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
				throw new DaoException(Reason.ALREADY_EXISTS);
			}
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(rs);
			DaoHelper.close(p);
		}
	}

	public void removePatient(int id) throws DaoException {
		String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + ID_FIELD_NAME + "=?";

		PreparedStatement p = null;
		try {
			p = c.prepareStatement(sql);
			p.setInt(1, id);
			int count = p.executeUpdate();
			if (count == 0) {
				throw new DaoException(Reason.DOES_NOT_EXIST);
			}
		} catch (SQLException e) {
			throw new DaoException(Reason.CAUSE, e);
		} finally {
			DaoHelper.close(p);
		}
	}

}
