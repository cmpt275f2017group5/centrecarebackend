/**
 *  
 *  GeofenceDao.java
 *  
 *  Accesses fields in the "geofence" table.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.centrecare.entities.Geofence;
import org.centrecare.dao.DaoException;
import org.centrecare.dao.DaoException.Reason;
import org.centrecare.dao.DaoHelper;

public class GeofenceDao implements AutoCloseable {
	private static final String TABLE_NAME = "geofence";
    private static final String GEOFENCE_ID_FIELD_NAME = "geofence_id";
	private static final String PATIENT_ID_FIELD_NAME = "patient_id";
	private static final String LONGITUDE_FIELD_NAME = "longitude";
	private static final String LATITUDE_FIELD_NAME = "latitude";
	private static final String RADIUS_FIELD_NAME = "radius";

	private Connection c;

	public GeofenceDao() throws DaoException {
		c = DaoHelper.getConnection();
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void close() {
		DaoHelper.close(c);
	}

    public int createGeofence(int patient_id, Geofence gf) throws DaoException {
        String sql = "INSERT INTO " + TABLE_NAME + " VALUES(null,?,?,?,?)";

        PreparedStatement p = null;
        ResultSet rs = null;
        try {
            p = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            p.setInt(1, patient_id);
            p.setDouble(2, gf.getLongitude());
            p.setDouble(3, gf.getLatitude());
            p.setDouble(4, gf.getRadius());

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                System.out.println("NO ROWS UPDATED");
                throw new DaoException(Reason.UNKNOWN);
            }

            rs = p.getGeneratedKeys();
            if(rs.next()) {
                return rs.getInt(1);
            }
            
            System.out.println("NO RESULT SET");
            throw new DaoException(Reason.UNKNOWN); 
        } catch(SQLException e) {
            System.out.println(e.getErrorCode());
            if(e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
                throw new DaoException(Reason.ALREADY_EXISTS);
            }

            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }

    public Geofence getGeofence(int geofence_id, int patient_id) throws DaoException {
        String sql = "SELECT " + LONGITUDE_FIELD_NAME + ", " + LATITUDE_FIELD_NAME + ", " + RADIUS_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + GEOFENCE_ID_FIELD_NAME + "=? AND " + PATIENT_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        try {
            p = c.prepareStatement(sql);
            p.setInt(1, geofence_id);
            p.setInt(2, patient_id);

            rs = p.executeQuery();
            if(rs.next()) {
                Double longitude = rs.getDouble(LONGITUDE_FIELD_NAME);
                Double latitude = rs.getDouble(LATITUDE_FIELD_NAME);
                Double radius = rs.getDouble(RADIUS_FIELD_NAME);

                return new Geofence(longitude, latitude, radius);
            }

            throw new DaoException(Reason.DOES_NOT_EXIST);
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }
/*
    public List<Geofence> getGeofenceList(int patient_id) throws DaoException {
        String sql = "SELECT " + GEOFENCE_ID_FIELD_NAME + ", " + LONGITUDE_FIELD_NAME + ", " + LATITUDE_FIELD_NAME + ", " + RADIUS_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + PATIENT_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet = null;
        try {
            p = c.prepareStatement(sql);
            p.setInt(1, patient_id);

            rs = p.executeQuery();
            
            Map<int, Geofence> patientGeofences;
            while(rs.next()) {
                int geofence_id = rs.getInt(GEOFENCE_ID_FIELD_NAME);
                Double longitude = rs.getDouble(LONGITUDE_FIELD_NAME);
                Double latitude = rs.getDouble(LATITUDE_FIELD_NAME);
                Double radius = rs.getDouble(RADIUS_FIELD_NAME);

                patientGeofences.append(geofence_id, new Geofence(longitude, latitude, radius));
            }
        } catch() {
        } finally {
        }
    }
*/


    public void updateGeofence(int geofence_id, int patient_id, Geofence gf) throws DaoException {
        String sql = "UPDATE " + TABLE_NAME + " SET " + LONGITUDE_FIELD_NAME + "=?, " + LATITUDE_FIELD_NAME + "=?, " + RADIUS_FIELD_NAME + "=? WHERE " + GEOFENCE_ID_FIELD_NAME + "=? AND " + PATIENT_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        try {
            p = c.prepareStatement(sql);
            p.setDouble(1, gf.getLongitude());
            p.setDouble(2, gf.getLatitude());
            p.setDouble(3, gf.getRadius());
            p.setInt(4, geofence_id);
            p.setInt(5, patient_id);

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                throw new DaoException(Reason.UNKNOWN);
            }
        } catch(SQLException e) {
            if(e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
                e.printStackTrace();
                throw new DaoException(Reason.ALREADY_EXISTS);
            }

            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(p);
        }
    }

    public void removeGeofence(int geofence_id, int patient_id) throws DaoException {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + GEOFENCE_ID_FIELD_NAME + "=? AND " + PATIENT_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        try {
            p = c.prepareStatement(sql);
            p.setInt(1, geofence_id);
            p.setInt(2, patient_id);

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                throw new DaoException(Reason.DOES_NOT_EXIST);
            }
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(p);
        }
    }
}
