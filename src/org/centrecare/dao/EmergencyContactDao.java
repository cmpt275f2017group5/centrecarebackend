/**
 *  
 *  EmergencyContactDao.java
 *  
 *  Accesses fields in the "contacts" table.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.centrecare.dao.DaoException.Reason;
import org.centrecare.entities.EmergencyContact;

public class EmergencyContactDao implements AutoCloseable {
	private static final String TABLE_NAME = "contact";
    private static final String ID_FIELD_NAME = "contact_id";
    private static final String PATIENT_ID_FIELD_NAME = "patient_id";
	private static final String FIRSTN_FIELD_NAME = "first_name";
	private static final String LASTN_FIELD_NAME = "last_name";
	private static final String PHONENO_FIELD_NAME = "phone_number";
    private static final String EMAIL_FIELD_NAME = "email";

	private Connection c;

	public EmergencyContactDao() throws DaoException {
		c = DaoHelper.getConnection();
	}

	public void setConnection(Connection connection) {
		c = connection;
	}

	public void close() {
		DaoHelper.close(c);
	}

    public int createEmergencyContact(int patient_id, EmergencyContact ec) throws DaoException {
        String sql = "INSERT INTO " + TABLE_NAME + " VALUES(null,?,?,?,?)";

        PreparedStatement p = null;
        ResultSet rs = null;
        try {
            p = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            p.setInt(1, patient_id);
            p.setString(2, ec.getFirstName());
            p.setString(3, ec.getLastName());
            p.setString(4, ec.getPhoneNumber());
            p.setString(5, ec.getEmail());

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                throw new DaoException(Reason.UNKNOWN);
            }

            rs = p.getGeneratedKeys();
            if(rs.next()) {
                return rs.getInt(1);
            }

            throw new DaoException(Reason.UNKNOWN);
        } catch(SQLException e) {
            if(e.getErrorCode() == DaoHelper.ER_DUP_ENTRY) {
                throw new DaoException(Reason.ALREADY_EXISTS);
            }

            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }

    public EmergencyContact getEmergencyContact(int patient_id, int contact_id) throws DaoException {
        String sql = "SELECT " + FIRSTN_FIELD_NAME + ", " + LASTN_FIELD_NAME + ", " + PHONENO_FIELD_NAME + ", " + EMAIL_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + ID_FIELD_NAME + "=? AND " + PATIENT_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        try {
            p = c.prepareStatement(sql);
            p.setInt(1, contact_id);
            p.setInt(2, patient_id);

            rs = p.executeQuery();
            if(rs.next()) {
                String firstName = rs.getString(FIRSTN_FIELD_NAME);
                String lastName = rs.getString(LASTN_FIELD_NAME);
                String phoneNo = rs.getString(PHONENO_FIELD_NAME);
                String email = rs.getString(EMAIL_FIELD_NAME);

                return new EmergencyContact(firstName, lastName, phoneNo, email);
            }

            throw new DaoException(Reason.DOES_NOT_EXIST);
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }
	
    public List<Integer> getEmergencyContactList(int patient_id) throws DaoException {
        String sql = "SELECT " + ID_FIELD_NAME + " FROM " + TABLE_NAME + " WHERE " + PATIENT_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        List<Integer> patientList = new ArrayList<Integer>();
        try {
            p = c.prepareStatement(sql);
            p.setInt(1, patient_id);
            
            rs = p.executeQuery();
            while(rs.next()) {
                patientList.add(rs.getInt(ID_FIELD_NAME));
            }

            return patientList;
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }

    public void updateEmergencyContact(int patient_id, int contact_id, EmergencyContact ec) throws DaoException {
        String sql = "UPDATE " + TABLE_NAME + " SET " + FIRSTN_FIELD_NAME + "=?, " + LASTN_FIELD_NAME + "=?, " + PHONENO_FIELD_NAME + "=?, " + EMAIL_FIELD_NAME + "=? WHERE " + ID_FIELD_NAME + "=? AND " + PATIENT_ID_FIELD_NAME + "=?";

        PreparedStatement p = null;
        ResultSet rs = null;
        try {
            p = c.prepareStatement(sql);
            p.setString(1, ec.getFirstName());
            p.setString(2, ec.getLastName());
            p.setString(3, ec.getPhoneNumber());
            p.setString(4, ec.getEmail());

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                throw new DaoException(Reason.DOES_NOT_EXIST);
            }
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(rs);
            DaoHelper.close(p);
        }
    }

    public void removeEmergencyContact(int patient_id, int contact_id) throws DaoException {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + ID_FIELD_NAME + "=? AND " + PATIENT_ID_FIELD_NAME + "=?";
        
        PreparedStatement p = null;
        try {
            p = c.prepareStatement(sql);
            p.setInt(1, contact_id);
            p.setInt(2, patient_id);

            int rowsUpdated = p.executeUpdate();
            if(rowsUpdated == 0) {
                throw new DaoException(Reason.DOES_NOT_EXIST);
            }
        } catch(SQLException e) {
            throw new DaoException(Reason.CAUSE, e);
        } finally {
            DaoHelper.close(p);
        }
    }
}
