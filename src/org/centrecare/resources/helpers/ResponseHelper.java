/**
 *  
 *  ResponseHelper.java
 *  
 *  Helper class that builds header "X-Result-Reason" for server responses. 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.resources.helpers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.core.Response.StatusType;
import javax.ws.rs.core.UriInfo;

import org.centrecare.entities.SessionContext;
import org.centrecare.services.ServiceException;
import org.centrecare.services.SessionService;

public class ResponseHelper {
	public static ResponseBuilder buildCustomResponse(Status status, String phrase, Object entity) {
		return Response.status(createCustomStatus(status, phrase)).entity(entity).header("X-Result-Reason", phrase);
	}

	public static ResponseBuilder buildCustomResponse(Status status, String phrase) {
		return buildCustomResponse(status, phrase, "");
	}

	public static StatusType createCustomStatus(Status status, String phrase) {
		return new StatusType() {
			@Override
			public int getStatusCode() {
				return status.getStatusCode();
			}

			@Override
			public String getReasonPhrase() {
				return phrase;
			}

			@Override
			public Family getFamily() {
				return status.getFamily();
			}
		};
	}

    //public static SessionContext

	public static SessionContext getSessionContext(String accessToken) throws ServiceException {
		SessionContext sc = SessionService.getSessionContext(accessToken);
		SessionService.updateAccessTime(accessToken);
		return sc;
	}

	public static String formLinkBody(UriInfo uriInfo, String linkPath, String relation) {
		return "<" + uriInfo.getBaseUri() + linkPath + ">; rel=\"" + relation + "\"";
	}

	public static String formLocationBody(UriInfo uriInfo, String linkPath) {
		return uriInfo.getBaseUri() + linkPath;
	}

}
