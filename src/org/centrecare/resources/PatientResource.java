/**
 *  
 *  PatientResource.java
 *  
 *  REST resource manager that handles path "api/patient" 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.centrecare.entities.EmergencyContact;
import org.centrecare.entities.Geofence;
import org.centrecare.entities.Patient;
import org.centrecare.entities.PatientLastLocation;
import org.centrecare.entities.PatientLocation;
import org.centrecare.entities.SessionContext;
import org.centrecare.entities.UserCredentials;
import org.centrecare.resources.helpers.ResponseHelper;
import org.centrecare.services.CaretakerService;
import org.centrecare.services.PatientService;
import org.centrecare.services.SessionService;
import org.centrecare.services.ServiceException;
import org.centrecare.types.UserRole;

@Path("/patient")
public class PatientResource {
    //@Context
    //UriInfo uriInfo;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postPatient(@HeaderParam("X-Access-Token") String accessToken, @HeaderParam("X-Pairing-Code") String pairCode, UserCredentials uc) {
        //Check for duplicate username
        if(SessionService.checkUsernameExists(uc.getUsername())) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Username already taken").build();
        }
        
        int cid;
        try {
			//Dereference Pairing Code
            cid = CaretakerService.usePairingCode(pairCode);
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Invalid Pairing Code").build();

            default:
                return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
            }
        }

        try {
            int pid = PatientService.createDefaultPatient(cid);
			SessionService.createCredentials(uc.getUsername(), uc.getPassword(), UserRole.PATIENT, pid);
		} catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
		}

        return ResponseHelper.buildCustomResponse(Status.OK, "Account created").build();
	}

	@Path("/{token}")
	@DELETE
	public Response deletePatient(@PathParam("token") String token, @HeaderParam("X-Access-Token") String accessToken) {
		return ResponseHelper.buildCustomResponse(Status.NOT_IMPLEMENTED, "Account deletion not available").build();
	}

	@Path("/{pid}/location")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocation(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken) {
		SessionContext sc;
		try {
			sc = ResponseHelper.getSessionContext(accessToken);
		} catch (ServiceException e) {
			return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
		}

		// XXX need to check if allowed to get patient's location

		// Check if account has priveledge
		if (!PatientService.hasResourceAccess(sc, patientId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
		}

		try {
			PatientLastLocation location = PatientService.getLocation(patientId);
			return ResponseHelper.buildCustomResponse(Status.OK, "Location avaiable", location).build();
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Location information not found").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
			}
		}
	}

	@Path("/{pid}/location")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putPatient(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken,
			PatientLocation location) {
		SessionContext sc;
		try {
			sc = ResponseHelper.getSessionContext(accessToken);
		} catch (ServiceException e) {
			return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
		}

		// XXX need to check if allowed to get patient's location

		// Check if account has priveledge
		if (!PatientService.hasResourceAccess(sc, patientId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
		}

		try {
			PatientService.updateLocation(patientId, location);
			return ResponseHelper.buildCustomResponse(Status.OK, "Location updated", location).build();
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Location information not found").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
			}
		}
	}

	@Path("/{pid}/profile")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPatient(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken) {
		SessionContext sc;
		try {
			sc = ResponseHelper.getSessionContext(accessToken);
		} catch (ServiceException e) {
			return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
		}

		// XXX need to check if allowed to get patient's location

		// Check if account has priveledge
		if (!PatientService.hasResourceAccess(sc, patientId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
		}

		try {
			Patient patient = PatientService.getPatient(patientId);
			return ResponseHelper.buildCustomResponse(Status.OK, "Profile found", patient).build();
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Patient information not found").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
			}
		}
	}

	@Path("/{pid}/profile")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePatient(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken,
			Patient patient) {
		SessionContext sc;
		try {
			sc = ResponseHelper.getSessionContext(accessToken);
		} catch (ServiceException e) {
			return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
		}

		// Check if account has priveledge
		if (!PatientService.hasResourceAccess(sc, patientId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
		}

		try {
			PatientService.updatePatient(patientId, patient);
			return ResponseHelper.buildCustomResponse(Status.OK, "Profile updated").build();
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Location information not found").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
			}
		}
	}

    @Path("/{pid}/geofence")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGeofenceList(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        return ResponseHelper.buildCustomResponse(Status.NOT_IMPLEMENTED, "Feature not yet implemented").build();
    }

    @Path("/{pid}/geofence")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createGeofence(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken, Geofence gf) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            int geofenceId = PatientService.createGeofence(patientId, gf);
            //TODO IMPLEMENT PROPERLY
            //String geofenceLinkBody = ResponseHelper.create"/api/" + patientId.toString() + "/geofence/" + geofenceId.toString()
            return ResponseHelper.buildCustomResponse(Status.OK, "Geofence created").header("X-GID", String.valueOf(geofenceId)).build();//header("Link", 
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Geofence already exists").build();
                
            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }

    @Path("/{pid}/geofence/{gid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGeofence(@PathParam("pid") int patientId, @PathParam("gid") int geofenceId, @HeaderParam("X-Access-Token") String accessToken) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            Geofence gf = PatientService.getGeofence(geofenceId, patientId);
            return ResponseHelper.buildCustomResponse(Status.OK, "Geofence found", gf).build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Invalid Geofence ID or Geofence ID not associated with this user").build();
            
            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }

    @Path("/{pid}/geofence/{gid}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateGeofence(@PathParam("pid") int patientId, @PathParam("gid") int geofenceId, @HeaderParam("X-Access-Token") String accessToken, Geofence gf) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            PatientService.updateGeofence(geofenceId, patientId, gf);
            return ResponseHelper.buildCustomResponse(Status.OK, "Geofence updated").build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Geofence already exists").build();

            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }

    @Path("/{pid}/geofence/{gid}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteGeofence(@PathParam("pid") int patientId, @PathParam("gid") int geofenceId, @HeaderParam("X-Access-Token") String accessToken) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            PatientService.removeGeofence(geofenceId, patientId);
            return ResponseHelper.buildCustomResponse(Status.OK, "Geofence Deleted").build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Geofence does not exist").build();

            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }

    @Path("/{pid}/contact")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmergencyContactList(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        return ResponseHelper.buildCustomResponse(Status.NOT_IMPLEMENTED, "Not yet implemented").build();
    }

    @Path("/{pid}/contact")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createEmergencyContact(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken, EmergencyContact ec) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            int contactId = PatientService.createEmergencyContact(patientId, ec);
            return ResponseHelper.buildCustomResponse(Status.OK, "Emergency Contact created").header("X-EID", String.valueOf(contactId)).build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Emergency Contact not found").build();

            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }

    @Path("/{pid}/contact/{eid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmergencyContact(@PathParam("pid") int patientId, @PathParam("eid") int contactId, @HeaderParam("X-Access-Token") String accessToken) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            EmergencyContact eg = PatientService.getEmergencyContact(patientId, contactId);
            return ResponseHelper.buildCustomResponse(Status.OK, "Emergency Contact found", eg).build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Emergency Contact not found").build();

            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }

    @Path("/{pid}/contact/{eid}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEmergencyContact(@PathParam("pid") int patientId, @PathParam("eid") int contactId, @HeaderParam("X-Access-Token") String accessToken, EmergencyContact eg) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            PatientService.updateEmergencyContact(patientId, contactId, eg);
            return ResponseHelper.buildCustomResponse(Status.OK, "Emergency Contact updated").build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Emergency Contact not found").build();

            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }

    @Path("/{pid}/contact/{eid}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteEmergencyContact(@PathParam("pid") int patientId, @PathParam("eid") int contactId, @HeaderParam("X-Access-Token") String accessToken) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!PatientService.hasResourceAccess(sc, patientId)) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        try {
            PatientService.removeEmergencyContact(patientId, contactId);
            return ResponseHelper.buildCustomResponse(Status.OK, "Emergency Contact deleted").build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Emergency Contact not found").build();

            default:
                return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
            }
        }
    }
/*
    @Path("/{pid}/picture")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPatientPicture(@PathParam("pid") int patientId, @HeaderParam("X-Access-Token") String accessToken, String picture) {
  */      
}
