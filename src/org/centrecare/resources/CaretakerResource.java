/**
 *  
 *  CaretakerResource.java
 *  
 *  w
 *  REST resource manager that handles path "api/caretaker" 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.resources;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.centrecare.entities.Caretaker;
import org.centrecare.entities.SessionContext;
import org.centrecare.entities.UserCredentials;
import org.centrecare.resources.helpers.ResponseHelper;
import org.centrecare.services.CaretakerService;
import org.centrecare.services.ServiceException;
import org.centrecare.services.SessionService;
import org.centrecare.types.UserRole;
import org.centrecare.types.UserStatus;

@Path("/caretaker")
public class CaretakerResource {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postCaretaker(@HeaderParam("X-Access-Token") String accessToken, UserCredentials uc) {
		//Check for duplicate username
        if(SessionService.checkUsernameExists(uc.getUsername())) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Username already taken").build();
        }
        
        try {
			int cid = CaretakerService.createDefaultCaretaker();
			SessionService.createCredentials(uc.getUsername(), uc.getPassword(), UserRole.CARETAKER, cid);
		} catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Error - " + e.getReason()).build();
		}

        return ResponseHelper.buildCustomResponse(Status.OK, "Account created").build();
	}

	@Path("/{pid}")
	@DELETE
	public Response deleteCaretaker(@PathParam("pid") String token,
			@HeaderParam("X-Access-Token") String accessToken) {
		return ResponseHelper.buildCustomResponse(Status.NOT_IMPLEMENTED, "Account deletion not available").build();
	}

	@Path("/{cid}/profile")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCaretaker(@PathParam("cid") int caretakerId, @HeaderParam("X-Access-Token") String accessToken) {
		SessionContext sc;
		try {
			sc = ResponseHelper.getSessionContext(accessToken);
		} catch (ServiceException e) {
			return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
		}

		// XXX need to check if allowed to get caretaker's location

		// Check if accoutn has priveledge
		if (!CaretakerService.hasResourceAccess(sc, caretakerId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
		}

		try {
			Caretaker caretaker = CaretakerService.getCaretaker(caretakerId);
			return ResponseHelper.buildCustomResponse(Status.OK, "Profile found", caretaker).build();
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Location information not found").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
			}
		}
	}

	@Path("/{cid}/profile")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCaretaker(@PathParam("cid") int caretakerId,
			@HeaderParam("X-Access-Token") String accessToken, Caretaker caretaker) {
		SessionContext sc;
		try {
			sc = ResponseHelper.getSessionContext(accessToken);
		} catch (ServiceException e) {
			return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
		}

		// XXX need to check if allowed to get caretaker's location

		// Check if accoutn has priveledge
		if (!CaretakerService.hasResourceAccess(sc, caretakerId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
		}

		try {
			CaretakerService.updateCaretaker(caretakerId, caretaker);
			return ResponseHelper.buildCustomResponse(Status.OK, "Profile updated").build();
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "Location information not found").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
			}
		}
	}

    @Path("/{cid}/pairing")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPairingCode(@PathParam("cid") int caretakerId, @HeaderParam("X-Access-Token") String accessToken) {
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!CaretakerService.hasResourceAccess(sc, caretakerId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        //Try to create new code
        String pairCode = null;
        try {
            pairCode = CaretakerService.createPairingCode(caretakerId);
            return ResponseHelper.buildCustomResponse(Status.OK, "Pairing code generated").header("X-Pairing-Code", pairCode).build();
        } catch(ServiceException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                //Handled below
                break;

            default:
                return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
            }
        }
        
        //Try to fetch code
        try {
            pairCode = CaretakerService.getPairingCode(caretakerId);
            return ResponseHelper.buildCustomResponse(Status.OK, "Access Code already generated previously").header("X-Pairing-Code", pairCode).build(); 
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }
    }

    @Path("/{cid}/plist")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPatientIdList(@PathParam("cid") int caretakerId, @HeaderParam("X-Access-Token") String accessToken) {
        System.out.println("In plist func");
        
        SessionContext sc;
        try {
            sc = ResponseHelper.getSessionContext(accessToken);
        } catch(ServiceException e) {
            return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason()).build();
        }

        if(!CaretakerService.hasResourceAccess(sc, caretakerId)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session").build();
        }

        //Try to get patient list
        List<Integer> plist = null;
        try {
            plist = CaretakerService.getPatientList(caretakerId);
            if(!plist.isEmpty()) {
                return ResponseHelper.buildCustomResponse(Status.OK, "Patient list created", plist).build();
            }
        } catch(ServiceException e) {
            //Fall through
        }
            
        return ResponseHelper.buildCustomResponse(Status.NOT_FOUND, "No patients associated with account").build();
    }
}
