/**
 *  
 *  SessionResources.java
 *  
 *  REST resource manager that handles path "api/session" 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.centrecare.entities.Session;
import org.centrecare.entities.SessionContext;
import org.centrecare.entities.UserCredentials;
import org.centrecare.resources.helpers.ResponseHelper;
import org.centrecare.services.ServiceException;
import org.centrecare.services.SessionService;
import org.centrecare.types.UserRole;

@Path("/session")
public class SessionResource {
	@Context
	UriInfo uriInfo;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postSession(@HeaderParam("X-Access-Token") String oldAccessToken, UserCredentials userCredentials) {
		// XXX what if client has Access Token in request?

		// XXX Fix later
		try {
			Session session = SessionService.createSession(userCredentials.getUsername(),
					userCredentials.getPassword());

			SessionContext sc = SessionService.getSessionContext(session.getAccessToken());

			switch (session.getUserRole()) {
			case CARETAKER: {
				String sessionLinkBody = ResponseHelper.formLinkBody(uriInfo, "session/" + session.getAccessToken(),
						"session");
				String sessionLocationBody = ResponseHelper.formLocationBody(uriInfo,
						"session/" + sc.getUserId() + session.getAccessToken());
				String profileLinkBody = ResponseHelper.formLinkBody(uriInfo,
						"caretaker/" + sc.getUserId() + "/profile", "profile");
				return ResponseHelper.buildCustomResponse(Status.OK, "User validated", session)
						.header("Link", sessionLinkBody).header("Link", profileLinkBody)
						.header("Location", sessionLocationBody).header("X-Access-Token", session.getAccessToken())
						.header("X-CID", String.valueOf(sc.getUserId())).build();
			}

			case PATIENT: {
				String sessionLinkBody = ResponseHelper.formLinkBody(uriInfo, "session/" + session.getAccessToken(),
						"session");
				String profileLinkBody = ResponseHelper.formLinkBody(uriInfo, "patient/" + sc.getUserId() + "/profile",
						"profile");
				String locationLinkBody = ResponseHelper.formLinkBody(uriInfo,
						"patient/" + sc.getUserId() + "/location", "location");
				String sessionLocationBody = ResponseHelper.formLocationBody(uriInfo,
						"session/" + sc.getUserId() + session.getAccessToken());
				return ResponseHelper.buildCustomResponse(Status.OK, "User validated", session)
						.header("Link", sessionLinkBody).header("Link", profileLinkBody)
						.header("Link", locationLinkBody).header("Location", sessionLocationBody)
						.header("X-Access-Token", session.getAccessToken()).header("X-PID", String.valueOf(sc.getUserId())).build();
			}

			default:
				return ResponseHelper.buildCustomResponse(Status.NOT_IMPLEMENTED, "Admin support not implemented")
						.build();

			}
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case USERNAME_PASSWORD_INVALID:
				return ResponseHelper.buildCustomResponse(Status.UNAUTHORIZED, "Invalid username or password").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSession(@HeaderParam("X-Access-Token") String accessToken) {
		SessionContext sessionContext;
		try {
			SessionService.updateAccessTime(accessToken);
			sessionContext = SessionService.getSessionContext(accessToken);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.UNAUTHORIZED, "Invalid Access Token").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		Session session;
		try {
			session = SessionService.getSession(sessionContext);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		ResponseBuilder builder = Response.status(Status.OK).entity(session);
		return builder.build();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteSession(@HeaderParam("X-Access-Token") String accessToken) {
		try {
			SessionService.getSessionContext(accessToken);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.UNAUTHORIZED, "Invalid Access Token").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		try {
			SessionService.destorySessionContext(accessToken);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		return ResponseHelper.buildCustomResponse(Status.OK, "Session destroyed").build();
	}

	@Path("/{token}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSession(@PathParam("token") String token, @HeaderParam("X-Access-Token") String accessToken) {
		SessionContext sessionContext;

		try {
			SessionService.updateAccessTime(accessToken);
			sessionContext = SessionService.getSessionContext(accessToken);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.UNAUTHORIZED, "Invalid Access Token").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		// regular users can only access their own session information
		if (sessionContext.getUserRole() != UserRole.ADMIN && !accessToken.equals(token)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to access other user's session")
					.build();
		}

		Session session;
		try {
			session = SessionService.getSession(sessionContext);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		ResponseBuilder builder = Response.status(Status.OK).entity(session);
		return builder.build();
	}

	@Path("/{token}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteSession(@PathParam("token") String token, @HeaderParam("X-Access-Token") String accessToken) {
		SessionContext sessionContext;

		try {
			sessionContext = SessionService.getSessionContext(accessToken);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				return ResponseHelper.buildCustomResponse(Status.UNAUTHORIZED, "Invalid Access Token").build();

			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		// regular users can only delete their own session information
		if (sessionContext.getUserRole() != UserRole.ADMIN && !accessToken.equals(token)) {
			return ResponseHelper.buildCustomResponse(Status.FORBIDDEN, "Not permitted to delete other user's session")
					.build();
		}

		try {
			SessionService.destorySessionContext(token);
		} catch (ServiceException e) {
			switch (e.getReason()) {
			default:
				return ResponseHelper.buildCustomResponse(Status.SERVICE_UNAVAILABLE, "Error - " + e.getReason())
						.build();
			}
		}

		return ResponseHelper.buildCustomResponse(Status.OK, "Session destroyed").build();
	}
}
