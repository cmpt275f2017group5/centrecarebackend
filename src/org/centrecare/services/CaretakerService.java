/**
 *  
 *  CaretakerService.java
 *  
 *  Functions regarding Caretakers. 
 *
 *  @author     First Last
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.services;

import java.util.List;
import java.util.UUID;

import org.centrecare.dao.CaretakerDao;
import org.centrecare.dao.DaoException;
import org.centrecare.dao.PairingCodeDao;
import org.centrecare.dao.PatientDao;
import org.centrecare.entities.Caretaker;
import org.centrecare.entities.SessionContext;
import org.centrecare.services.ServiceException.Reason;
import org.centrecare.types.UserRole;

public class CaretakerService {

	public static Caretaker getCaretaker(int caretakerId) throws ServiceException {
		try (CaretakerDao dao = new CaretakerDao()) {
			return dao.getCaretaker(caretakerId);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static int createCaretaker(Caretaker caretaker) throws ServiceException {
		try (CaretakerDao careDao = new CaretakerDao()) {
			int cid = careDao.createCaretaker(caretaker);
            return cid;       
		} catch (DaoException e) {
			switch (e.getReason()) {
			case ALREADY_EXISTS:
				throw new ServiceException(Reason.ALREADY_EXISTS);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

    public static int createDefaultCaretaker() throws ServiceException {
        try (CaretakerDao careDao = new CaretakerDao()) {
            Caretaker c = Caretaker.defaultCaretaker();
            int cid = careDao.createCaretaker(c);
            return cid;
        } catch (DaoException e) {
            switch (e.getReason()) {
            case ALREADY_EXISTS:
                throw new ServiceException(Reason.ALREADY_EXISTS);

            default:
                e.printStackTrace();
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

	public static void updateCaretaker(int caretakerId, Caretaker caretaker) throws ServiceException {
		try (CaretakerDao dao = new CaretakerDao()) {
			dao.updateCaretaker(caretakerId, caretaker);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void removeCaretaker(int caretakerId) throws ServiceException {
		try (CaretakerDao dao = new CaretakerDao()) {
			dao.removeCaretaker(caretakerId);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);


			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

    public static String createPairingCode(int caretakerId) throws ServiceException {
        String pairCode = UUID.randomUUID().toString();
        try (PairingCodeDao dao = new PairingCodeDao()) {
            dao.createPairingCode(pairCode, caretakerId);
            return pairCode;
        } catch(DaoException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                throw new ServiceException(Reason.ALREADY_EXISTS);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static String getPairingCode(int caretakerId) throws ServiceException {
        try (PairingCodeDao dao = new PairingCodeDao()) {
            return dao.getPairingCode(caretakerId);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                throw new ServiceException(Reason.ALREADY_EXISTS);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }    

    public static int usePairingCode(String pairCode) throws ServiceException {
        try (PairingCodeDao dao = new PairingCodeDao()) {
            return dao.usePairingCode(pairCode);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                throw new ServiceException(Reason.DOES_NOT_EXIST);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static boolean hasResourceAccess(SessionContext sc, int path_id) {
        return  sc.getUserRole() == UserRole.ADMIN ||
                (sc.getUserRole() == UserRole.CARETAKER && sc.getUserId() == path_id);
    }

    public static List<Integer> getPatientList(int caretakerId) throws ServiceException {
        try (PatientDao dao = new PatientDao()) {
            return dao.getPatientIdList(caretakerId);
        } catch(DaoException e) {
            throw new ServiceException(Reason.UNKNOWN);
        }
   }
}
