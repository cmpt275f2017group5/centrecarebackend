/**
 *  
 *  ConfigService.java
 *  
 *  Provides application configuration values 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

public class ConfigService {
	private static final String PROPERTIES_FILE = "config.properties";
	private static final Properties properties = new Properties();

	static {
		try (InputStream is = Class.class.getResourceAsStream(PROPERTIES_FILE)) {
			properties.load(is);
		} catch (IOException e) {
			System.err.println("Failed to load configuration data");
		}
	}

	public static String get(String key) {
		return properties.getProperty(key);
	}

	public static Optional<Integer> getInteger(String key) {
		String str = properties.getProperty(key);
		if (str == null) {
			return Optional.empty();
		}
		try {
			return Optional.of(Integer.parseInt(str));
		} catch (NumberFormatException e) {
			return Optional.empty();
		}
	}

	public static int getInteger(String key, int defaultValue) {
		String str = properties.getProperty(key);
		if (str == null) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static Optional<Long> getLong(String key) {
		String str = properties.getProperty(key);
		if (str == null) {
			return Optional.empty();
		}
		try {
			return Optional.of(Long.parseLong(str));
		} catch (NumberFormatException e) {
			return Optional.empty();
		}
	}

	public static long getLong(String key, long defaultValue) {
		String str = properties.getProperty(key);
		if (str == null) {
			return defaultValue;
		}
		try {
			return Long.parseLong(str);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static Optional<Boolean> getBoolean(String key) {
		String str = properties.getProperty(key);
		if (str == null) {
			return Optional.empty();
		}
		return Optional.of(Boolean.parseBoolean(str));
	}

	public static boolean getBoolean(String key, boolean defaultValue) {
		String str = properties.getProperty(key);
		if (str == null) {
			return defaultValue;
		}
		return Boolean.parseBoolean(str);
	}

}
