/**
 *  
 *  SessionService.java
 *  
 *  Manages session for currently logged-in users. 
 *
 *  @author     First Last
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.services;

import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.centrecare.dao.CredentialsDao;
import org.centrecare.dao.DaoException;
import org.centrecare.dao.SessionContextDao;
import org.centrecare.entities.Caretaker;
import org.centrecare.entities.Patient;
import org.centrecare.entities.Session;
import org.centrecare.entities.SessionContext;
import org.centrecare.services.ServiceException.Reason;
import org.centrecare.types.UserRole;
import org.centrecare.types.UserStatus;

public class SessionService {

	public static SessionContext getSessionContext(String accessToken) throws ServiceException {
		try (SessionContextDao dao = new SessionContextDao()) {
			return dao.getSessionContext(accessToken);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static SessionContext createSessionContext(String accessToken, UserRole userRole, int userId,
			Map<String, String> attributes) throws ServiceException {
		try (SessionContextDao dao = new SessionContextDao()) {
			// create new session
			dao.createSessionContext(accessToken, userRole, userId);

			// add attributes
			if (attributes != null) {
				for (Entry<String, String> entry : attributes.entrySet()) {
					dao.setAttribute(accessToken, entry.getKey(), entry.getValue());
				}
			}

			return getSessionContext(accessToken);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case ALREADY_EXISTS:
				throw new ServiceException(Reason.ALREADY_EXISTS);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void destorySessionContext(String accessToken) throws ServiceException {
		try (SessionContextDao dao = new SessionContextDao()) {
			dao.removeSessionContext(accessToken);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void updateAccessTime(String accessToken) throws ServiceException {
		try (SessionContextDao dao = new SessionContextDao()) {
			dao.updateLastAccess(accessToken);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static String getAttribute(String accessToken, String key) throws ServiceException {
		try (SessionContextDao dao = new SessionContextDao()) {
			return dao.getAttribute(accessToken, key);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			case NOT_FOUND:
				return null;

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void setAttribute(String accessToken, String key, String value) throws ServiceException {
		try (SessionContextDao dao = new SessionContextDao()) {
			dao.setAttribute(accessToken, key, value);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void removeAttribute(String accessToken, String key) throws ServiceException {
		try (SessionContextDao dao = new SessionContextDao()) {
			dao.removeAttribute(accessToken, key);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	private static String generateAccessToken() {
		return UUID.randomUUID().toString();
	}

	public static Session getSession(UserRole userRole, int userId) throws ServiceException {
		throw new ServiceException(Reason.UNKNOWN);
	}

	public static Session getSession(SessionContext sessionContext) throws ServiceException {
		try (SessionContextDao sessionDao = new SessionContextDao()) {
			String accessToken = sessionContext.getAccessToken();
			UserRole userRole = sessionContext.getUserRole();
			int userId = sessionContext.getUserId();

			String displayName = "Unknown";
			switch (userRole) {
			case PATIENT:
				Patient p = PatientService.getPatient(userId);
				displayName = p.getDisplayName();
				break;

			case CARETAKER:
				 Caretaker ct = CaretakerService.getCaretaker(userId);
				 displayName = ct.getDisplayName();
				break;

			default:
			}

			// XXX form real name
			// XXX expiry time should not be hard-coded
			return new Session(userRole, displayName, accessToken, 1800);
		} catch (DaoException e) {
			switch (e.getReason()) {
			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static Session createSession(String username, String password) throws ServiceException {
		try (CredentialsDao credDao = new CredentialsDao(); SessionContextDao sessionDao = new SessionContextDao()) {
			if (!credDao.validate(username, password)) {
				throw new ServiceException(Reason.USERNAME_PASSWORD_INVALID);
			}

			UserRole userRole = credDao.getUserRole(username);
			int userId = credDao.getUserId(username);

			// remove any pre-existing session contexts for this user
			sessionDao.removeAllSessionContexts(userRole, userId);

			// create new session context
			String accessToken = generateAccessToken();
			sessionDao.createSessionContext(accessToken, userRole, userId);

			SessionContext sessionContext = sessionDao.getSessionContext(accessToken);
			return getSession(sessionContext);
		} catch (DaoException e) {
			switch (e.getReason()) {
			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void checkSessionExpired(long period) {
		try (SessionContextDao dao = new SessionContextDao()) {
			dao.expireSessions(period);
		} catch (DaoException e) {
			// silently ignore
		}
	}

	public static void createCredentials(String username, String password, UserRole userRole, int userId) throws ServiceException {
		try (CredentialsDao credDao = new CredentialsDao()) {
			//Should create an account with type "NOT_READY"
			credDao.createCredentials(username, password, userRole, userId, UserStatus.READY);
        } catch(DaoException e) {
			switch(e.getReason()) {
			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

    public static boolean checkUsernameExists(String username) {
        try (CredentialsDao credDao = new CredentialsDao()) {
            return credDao.checkUsernameExists(username);
        } catch(DaoException e) {
            return false;
            //throw new ServiceException(Reason.UNKNOWN);
        }
    }

}
