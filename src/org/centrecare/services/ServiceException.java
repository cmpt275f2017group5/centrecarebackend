/**
 *  
 *  ServiceException.java
 *  
 *  Exception pertaining to  
 *
 *  @author     First Last
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.services;

public class ServiceException extends Throwable {
	private static final long serialVersionUID = 1L;
	private final Reason reason;

	public ServiceException(Reason reason) {
		this.reason = reason;
	}

	public ServiceException(Reason reason, String message) {
		super(message);
		this.reason = reason;
	}

	public ServiceException(Reason reason, Throwable cause) {
		super(cause);
		this.reason = reason;
	}

	public ServiceException(Reason reason, String message, Throwable cause) {
		super(message, cause);
		this.reason = reason;
	}

	public Reason getReason() {
		return reason;
	}

	public enum Reason {
		UNKNOWN, DOES_NOT_EXIST, ALREADY_EXISTS, USERNAME_PASSWORD_INVALID;
	}

}
