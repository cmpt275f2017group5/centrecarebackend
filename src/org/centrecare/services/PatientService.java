/**
 *  
 *  PatientService.java
 *  
 *  Manages session for currently logged-in users. 
 *
 *  @author     First Last
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.services;

import java.util.List;

import org.centrecare.dao.DaoException;
import org.centrecare.dao.EmergencyContactDao;
import org.centrecare.dao.GeofenceDao;
import org.centrecare.dao.PatientDao;
import org.centrecare.dao.PatientLastLocationDao;
import org.centrecare.entities.EmergencyContact;
import org.centrecare.entities.Geofence;
import org.centrecare.entities.Patient;
import org.centrecare.entities.PatientLastLocation;
import org.centrecare.entities.PatientLocation;
import org.centrecare.entities.SessionContext;
import org.centrecare.services.ServiceException.Reason;
import org.centrecare.types.UserRole;

public class PatientService {

	public static PatientLastLocation getLocation(int patientId) throws ServiceException {
		try (PatientLastLocationDao dao = new PatientLastLocationDao()) {
			return dao.getLocation(patientId);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void updateLocation(int patientId, PatientLocation location) throws ServiceException {
		try (PatientLastLocationDao dao = new PatientLastLocationDao()) {
			dao.updateLocation(patientId, location);

            //Check location
		} catch (DaoException e) {
			System.out.println(e.getReason());
            switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static Patient getPatient(int patientId) throws ServiceException {
		try (PatientDao dao = new PatientDao()) {
			return dao.getPatient(patientId);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static int createPatient(Patient patient) throws ServiceException {
		try (PatientDao dao = new PatientDao()) {
			return dao.createPatient(patient);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case ALREADY_EXISTS:
				throw new ServiceException(Reason.ALREADY_EXISTS);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

    public static int createDefaultPatient(int caretakerId) throws ServiceException {
        try (PatientDao dao = new PatientDao()) {
            Patient p = Patient.defaultPatient(caretakerId);
            return dao.createPatient(p);
        } catch (DaoException e) {
            switch (e.getReason()) {
            case ALREADY_EXISTS:
                throw new ServiceException(Reason.ALREADY_EXISTS);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

	public static void updatePatient(int patientId, Patient patient) throws ServiceException {
		try (PatientDao dao = new PatientDao()) {
			dao.updatePatient(patientId, patient);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}

	public static void removePatient(int patientId) throws ServiceException {
		try (PatientDao dao = new PatientDao()) {
			dao.removePatient(patientId);
		} catch (DaoException e) {
			switch (e.getReason()) {
			case DOES_NOT_EXIST:
				throw new ServiceException(Reason.DOES_NOT_EXIST);

			default:
				throw new ServiceException(Reason.UNKNOWN);
			}
		}
	}


    public static int createGeofence(int patient_id, Geofence gf) throws ServiceException {
        try (GeofenceDao dao = new GeofenceDao()) {
            return dao.createGeofence(patient_id, gf);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                throw new ServiceException(Reason.ALREADY_EXISTS);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }


    public static Geofence getGeofence(int geofence_id, int patient_id) throws ServiceException {
        try (GeofenceDao dao = new GeofenceDao()) {
            return dao.getGeofence(geofence_id, patient_id);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                throw new ServiceException(Reason.DOES_NOT_EXIST);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static void updateGeofence(int geofence_id, int patient_id, Geofence gf) throws ServiceException {
        try (GeofenceDao dao = new GeofenceDao()) {
            dao.updateGeofence(geofence_id, patient_id, gf);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                throw new ServiceException(Reason.ALREADY_EXISTS);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static void removeGeofence(int geofence_id, int patient_id) throws ServiceException {
        try (GeofenceDao dao = new GeofenceDao()) {
            dao.removeGeofence(geofence_id, patient_id);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                throw new ServiceException(Reason.DOES_NOT_EXIST);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static boolean hasResourceAccess(SessionContext sc, int path_id) {
        //Check if account itself has permissions
        boolean accountPerms =
            sc.getUserRole() == UserRole.ADMIN ||
            (sc.getUserRole() == UserRole.PATIENT && sc.getUserId() == path_id);

        if(accountPerms) {
            return true;
        }

        //If the user isnt a caretaker he can't have patients
        if(sc.getUserRole() != UserRole.CARETAKER) {
            return false;
        }

        //Check if caretaker monitors this patient
        try (PatientDao dao = new PatientDao()) {
            List<Integer> patientList = dao.getPatientIdList(sc.getUserId());
           
            if(patientList.contains(path_id)) {
                return true;
            }

        } catch(DaoException e) {
            //Pass
        }

        return false;
    }

    public static int createEmergencyContact(int patient_id, EmergencyContact eg) throws ServiceException {
        try (EmergencyContactDao dao = new EmergencyContactDao()) {
            int contact_id = dao.createEmergencyContact(patient_id, eg);
            return contact_id;
        } catch(DaoException e) {
            switch(e.getReason()) {
            case ALREADY_EXISTS:
                throw new ServiceException(Reason.ALREADY_EXISTS);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static EmergencyContact getEmergencyContact(int patient_id, int contact_id) throws ServiceException {
        try (EmergencyContactDao dao = new EmergencyContactDao()) {
            EmergencyContact ec = dao.getEmergencyContact(patient_id, contact_id);
            return ec;
        } catch(DaoException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                throw new ServiceException(Reason.DOES_NOT_EXIST);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static List<Integer> getEmergencyContactList(int patient_id) throws ServiceException {
        try (EmergencyContactDao dao = new EmergencyContactDao()) {
            List<Integer> ids = dao.getEmergencyContactList(patient_id);
            return ids;
        } catch(DaoException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                throw new ServiceException(Reason.DOES_NOT_EXIST);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static void updateEmergencyContact(int patient_id, int contact_id, EmergencyContact eg) throws ServiceException {
        try (EmergencyContactDao dao = new EmergencyContactDao()) {
            dao.updateEmergencyContact(patient_id, contact_id, eg);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                throw new ServiceException(Reason.DOES_NOT_EXIST);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }

    public static void removeEmergencyContact(int patient_id, int contact_id) throws ServiceException {
        try (EmergencyContactDao dao = new EmergencyContactDao()) {
            dao.removeEmergencyContact(patient_id, contact_id);
        } catch(DaoException e) {
            switch(e.getReason()) {
            case DOES_NOT_EXIST:
                throw new ServiceException(Reason.DOES_NOT_EXIST);

            default:
                throw new ServiceException(Reason.UNKNOWN);
            }
        }
    }
}
