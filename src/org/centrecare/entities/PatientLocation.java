/**
 *  
 *  PatientLocation.java
 *  
 *  Represents the last reported location for a patient
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PatientLocation {
	@JsonProperty("longitude")
	private final Double longitude;

	@JsonProperty("latitude")
	private final Double latitude;

	public PatientLocation() {
		longitude= 0.0;
		latitude= 0.0;
	}
	
	public PatientLocation(Double longitude, Double latitude) {
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	@Override
	public String toString() {
		return "PatientLocation [longitude=" + longitude + ", latitude=" + latitude + "]";
	}

}
