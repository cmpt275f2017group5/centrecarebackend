/**
 *  
 *  UserCredentials.java
 *  
 *  Json object that is received from client when performing a login.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserCredentials {
	@JsonProperty("username")
	private String username;

	@JsonProperty("password")
	private String password;

    @SuppressWarnings("unused")
	private UserCredentials() {
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "UserCredentials [username=" + username + ", password=" + password + "]";
	}

}
