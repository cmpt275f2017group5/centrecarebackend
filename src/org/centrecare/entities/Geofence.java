/**
 *  
 *  Geofence.java
 *  
 *  Represents the geofence boundaries for a patient
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Geofence {
	@JsonProperty("longitude")
	private final Double longitude;

	@JsonProperty("latitude")
	private final Double latitude;

    @JsonProperty("radius")
    private final Double radius;

	public Geofence() {
		longitude= 0.0;
		latitude= 0.0;
        radius= 0.0;
	}
	
	public Geofence(Double longitude, Double latitude, Double radius) {
		this.longitude = longitude;
		this.latitude = latitude;
        this.radius = radius;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

    public Double getRadius() {
        return radius;
    }

	@Override
	public String toString() {
		return "PatientLocation [longitude=" + longitude + ", latitude=" + latitude + ", radius=" + radius + "]";
	}

}
