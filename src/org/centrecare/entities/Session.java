/**
 *  
 *  Session.java
 *  
 *  Json object framework that gets returned when a login is completed. 
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import org.centrecare.types.UserRole;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Session {
	@JsonProperty("user-role")
	private final UserRole userRole;

	@JsonProperty("display-name")
	private final String displayName;

	@JsonProperty("access-token")
	private final String accessToken;

	@JsonProperty("inactivity-expiry-period")
	private final long inactivityExpiryPeriod;

	public Session(UserRole userRole, String displayName, String accessToken, long inactivtyExpiryPeriod) {
		super();
		this.userRole = userRole;
		this.displayName = displayName;
		this.accessToken = accessToken;
		this.inactivityExpiryPeriod = inactivtyExpiryPeriod;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public long getInactivityExpiryPeriod() {
		return inactivityExpiryPeriod;
	}

	@Override
	public String toString() {
		return "Session [userRole=" + userRole + ", displayName=" + displayName + ", accessToken=" + accessToken
				+ ", inactivityExpiryPeriod=" + inactivityExpiryPeriod + "]";
	}

	
}
