/**
 *  
 *  Patient.java
 *  
 *  Object that stores a patient user's information.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.centrecare.entities.Address;

public class Patient extends User {
	@JsonProperty("date-of-birth")
	private Date dateofBirth;

	//@JsonProperty("caretaker-id")
	private int caretakerId;

	@SuppressWarnings("unused") // for JSON injector
	private Patient() {
		super();
	}

	public Patient(String firstName, String lastName, String phoneNo, Address address, Date dateofBirth,
			int caretakerId) {
		super(firstName, lastName, phoneNo, address);
		this.dateofBirth = dateofBirth;
		this.caretakerId = caretakerId;
	}

    public static Patient defaultPatient(int caretakerId) {
        return new Patient("First", "Last", "000-000-0000", Address.defaultAddress(), new Date(0L), caretakerId);
    }

	public Date getDateofBirth() {
		return dateofBirth;
	}

	public int getCaretakerId() {
		return caretakerId;
	}

	@Override
	public String toString() {
		return "Patient [dateofBirth=" + dateofBirth + ", caretakerId=" + caretakerId + "]";
	}

}
