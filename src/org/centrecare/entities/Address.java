/**
 *  
 *  Address.java
 *  
 *  Object that contains a user's address.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address {
	// private static String ADDR_REGEX = "^[ a-zA-Z0-9\\.-]+$";
	// private static String LOC_REGEX = "^[ a-zA-Z\\- ]+$";
	// private static String POSTAL_REGEX =
	// "^[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ]
	// ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$";

	@JsonProperty("street")
	private String street;

	@JsonProperty("city")
	private String city;

	@JsonProperty("province")
	private String province;

	@JsonProperty("country")
	private String country;

	@JsonProperty("postal-code")
	private String postalCode;

	@SuppressWarnings("unused") // for JSON injector
	private Address() {
	}
	
	public Address(String street, String city, String province, String country, String postalCode) {
		// boolean throwFlag =
		// street.matches(ADDR_REGEX) &&
		// city.matches(LOC_REGEX) &&
		// province.matches(LOC_REGEX) &&
		// country.matches(LOC_REGEX) &&
		// postalCode.matches(POSTAL_REGEX);
		//
		// if(!throwFlag) {
		// throw InvalidAddressException();
		// }
		//
		this.street = street;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode.toUpperCase();
	}

    public static Address defaultAddress() {
        return new Address("street", "city", "prov", "country", "postal");
    }

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public String getProvince() {
		return province;
	}

	public String getCountry() {
		return country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", city=" + city + ", province=" + province + ", country=" + country
				+ ", postalCode=" + postalCode + "]";
	}

}
