/**
 *  
 *  PatientLastLocation.java
 *  
 *  Represents the last reported location for a patient
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PatientLastLocation {
	@JsonProperty("longitude")
	private final Double longitude;

	@JsonProperty("latitude")
	private final Double latitude;

	@JsonProperty("timestamp")
	private final Date timestamp;

	public PatientLastLocation(Double longitude, Double latitude, Date timestamp) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.timestamp = timestamp;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return "PatientLastLocation [longitude=" + longitude + ", latitude=" + latitude + ", timestamp=" + timestamp
				+ "]";
	}

}
