/**
 *  
 *  SessionContext.java
 *  
 *  Stores information about a currently logged-in user.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.centrecare.types.UserRole;

public class SessionContext {
	private final String accessToken;
	private final UserRole userRole;
	private final int userId;
	private Date lastAccessTimestamp;
	private final Map<String, String> attribute;

	public SessionContext(String accessToken, UserRole userRole, int userId, Date lastAccessTime) {
		super();
		this.accessToken = accessToken;
		this.userRole = userRole;
		this.userId = userId;
		this.lastAccessTimestamp = lastAccessTime;
		attribute = new HashMap<>();
	}

	public SessionContext(String accessToken, String userRole, int userId, Date lastAccessTime) {
		this(accessToken, UserRole.valueOf(userRole), userId, lastAccessTime);
	}

	public String getAccessToken() {
		return accessToken;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public int getUserId() {
		return userId;
	}

	public Date getLastAccessTimestamp() {
		return lastAccessTimestamp;
	}

	public Map<String, String> getAttribute() {
		return attribute;
	}

	@Override
	public String toString() {
		return "SessionContext [accessToken=" + accessToken + ", userRole=" + userRole + ", userId=" + userId
				+ ", lastAccessTimestamp=" + lastAccessTimestamp + ", attribute=" + attribute + "]";
	}

	
}
