/**
 *  
 *  EmergencyContact.java
 *  
 *  Object that stores contact info for patient to be able to contact.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmergencyContact {
    @JsonProperty("first-name")
    private final String firstName;

    @JsonProperty("last-name")
    private final String lastName;

	@JsonProperty("phone-no")
    private final String phoneNo;

    @JsonProperty("email")
	private final String email;

	public EmergencyContact(String firstName, String lastName, String phoneNo, String email) {
		this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNo = phoneNo;
		this.email = email;
	}

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

	public String getPhoneNumber() {
		return phoneNo;
	}

	public String getEmail() {
		return email;
	}

    @Override
    public String toString() {
        return "EmergencyContact [firstName=" + firstName + ", lastName=" + lastName + ", phoneNo=" + phoneNo + ", email=" + email + "]";
    }
}
