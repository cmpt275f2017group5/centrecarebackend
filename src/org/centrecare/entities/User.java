/**
 *  
 *  User.java
 *  
 *  Base object for a user that contains information needed to create an account.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class User {
	@JsonProperty("first-name")
	protected String firstName;

	@JsonProperty("last-name")
	protected String lastName;

	@JsonProperty("phone-no")
	protected String phoneNo;

	@JsonProperty("address")
	protected Address address;

	// for JSON injector
	protected User() {
	}

	protected User(String firstName, String lastName, String phoneNo, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNo = phoneNo;
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public Address getAddress() {
		return address;
	}

	@JsonIgnore
	public String getDisplayName() {
		return firstName + " " + lastName;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", phoneNo=" + phoneNo + ", address="
				+ address + "]";
	}

}
