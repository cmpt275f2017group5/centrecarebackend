/**
 *  
 *  Caretaker.java
 *  
 *  Object that contains Caretaker information.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.centrecare.entities.Address;

public class Caretaker extends User {
	@JsonProperty("email")
	protected String email;

	@SuppressWarnings("unused") // for JSON injector
	private Caretaker() {
	}
	
	public Caretaker(String firstName, String lastName, String phoneNo, Address address, String email) {
		super(firstName, lastName, phoneNo, address);
		this.email = email;
	}

    public static Caretaker defaultCaretaker() {
        return new Caretaker("First", "Last", "000-000-0000", Address.defaultAddress(), "email");
    }

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "Caretaker [email=" + email + ", firstName=" + firstName + ", lastName=" + lastName + ", phoneNo="
				+ phoneNo + ", address=" + address + "]";
	}

}
