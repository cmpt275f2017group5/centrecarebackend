/**
 *  
 *  ApiSecurityFilter.java
 *  
 *  Filters requests needing authorization from those that do not
 *  Checks whether header X-Access-Token contains a vaild token
 *
 *  @author     First Last
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Response;
import org.centrecare.entities.SessionContext;
import org.centrecare.services.ServiceException;
import org.centrecare.services.SessionService;

@WebFilter(filterName = "ApiSecurity", urlPatterns = { "/api/*" })
public class ApiSecurityFilter implements Filter {
	private static String[] noAuthForPostUris = { "/session", "/patient", "/caretaker" };
	private static final List<String> noAuthForPost = new ArrayList<>(Arrays.asList(noAuthForPostUris));

	public ApiSecurityFilter() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String path = httpRequest.getPathInfo();
		String method = httpRequest.getMethod();
		String accessToken = httpRequest.getHeader("X-Access-Token");

		if (noAuthForPost.contains(path) && "POST".equals(method)) {
			// no access-token required
			request.getServletContext().log(method + " request for " + path + " does not require an Access Token");
			chain.doFilter(request, response);
			return;
		}

		// valid access-token required
		if (accessToken == null) {
			request.getServletContext().log(method + " request for " + path + " rejected - Access Token required");
			httpResponse.addHeader("X-Result-Reason", "Access Token required");
			httpResponse.setContentLength(0);
			httpResponse.sendError(Response.SC_UNAUTHORIZED, "Access Token required");
			return;
		}

		SessionContext sessionContext = null;
		try {
			sessionContext = SessionService.getSessionContext(accessToken);
		} catch (ServiceException e) {
			// silently ignore
		}
		if (sessionContext == null) {
			request.getServletContext()
					.log(method + " request for " + path + " rejected - Access Token '" + accessToken + "' invalid");
			httpResponse.addHeader("X-Result-Reason", "Access Token is invalid");
			httpResponse.setContentLength(0);
			httpResponse.sendError(Response.SC_FORBIDDEN, "Access Token is invalid");
			return;
		}

		// continue processing
		chain.doFilter(request, response);
		return;
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
