/**
 *  
 *  UserTypes.java
 *  
 *  Contains the different user types allowed.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.types;

public enum UserRole {
	PATIENT("Patient"), CARETAKER("Caretaker"), ADMIN("Administrator");

	private final String title;

	private UserRole(String title) {
		this.title = title;
	}

	public final String getTitle() {
		return title;
	}

}
