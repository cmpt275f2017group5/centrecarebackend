/**
 *  
 *  UserStatus.java
 *  
 *  Contains the different account status types.
 *
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare.types;

public enum UserStatus {
	READY("Ready"), NOT_READY("Not Ready"), DEACTIVATED("Deactivated");

	private final String title;

	private UserStatus(String title) {
		this.title = title;
	}

	public final String getTitle() {
		return title;
	}

}
