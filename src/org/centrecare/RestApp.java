/**
 *  
 *  RestApp.java
 *  
 *  Starting point for the Rest Application
 *   
 *  @author     Steven McLeod
 *  @version    1.0
 *  @since      1.0
 *  
 */

package org.centrecare;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api")
public class RestApp extends ResourceConfig {
	public RestApp() {
		register(JacksonFeature.class);
		packages("org.centrecare.resources");
	}
}
